#include <stdio.h>
#include <string.h>

#include <unistd.h>
#include <sys/stat.h>

#include "beargit.h"
#include "util.h"

/* Implementation Notes:
 *
 * - Functions return 0 if successful, 1 if there is an error.
 * - All error conditions in the function description need to be implemented
 *   and written to stderr. We catch some additional errors for you in main.c.
 * - Output to stdout needs to be exactly as specified in the function description.
 * - Only edit this file (beargit.c)
 * - Here are some of the helper functions from util.h:
 *   * fs_mkdir(dirname): create directory <dirname>
 *   * fs_rm(filename): delete file <filename>
 *   * fs_mv(src,dst): move file <src> to <dst>, overwriting <dst> if it exists
 *   * fs_cp(src,dst): copy file <src> to <dst>, overwriting <dst> if it exists
 *   * write_string_to_file(filename,str): write <str> to filename (overwriting contents)
 *   * read_string_from_file(filename,str,size): read a string of at most <size> (incl.
 *     NULL character) from file <filename> and store it into <str>. Note that <str>
 *     needs to be large enough to hold that string.
 *  - You NEED to test your code. The autograder we provide does not contain the
 *    full set of tests that we will run on your code. See "Step 5" in the project spec.
 */

/* beargit init
 *
 * - Create .beargit directory
 * - Create empty .beargit/.index file
 * - Create .beargit/.prev file containing 0..0 commit id
 *
 * Output (to stdout):
 * - None if successful
 */
  
int beargit_init(void) {
  fs_mkdir(".beargit");

  FILE* findex = fopen(".beargit/.index", "w");
  fclose(findex);

  FILE* fbranches = fopen(".beargit/.branches", "w");
  fprintf(fbranches, "%s\n", "master");
  fclose(fbranches);

  write_string_to_file(".beargit/.prev", "0000000000000000000000000000000000000000");
  write_string_to_file(".beargit/.current_branch", "master");

  return 0;
}



/* beargit add <filename>
 *
 * - Append filename to list in .beargit/.index if it isn't in there yet
 *
 * Possible errors (to stderr):
 * >> ERROR:  File <filename> has already been added.
 *
 * Output (to stdout):
 * - None if successful
 */

int beargit_add(const char* filename) {
  FILE* findex = fopen(".beargit/.index", "r");
  FILE *fnewindex = fopen(".beargit/.newindex", "w");

  char line[FILENAME_SIZE];
  while(fgets(line, sizeof(line), findex)) {
    strtok(line, "\n");
    if (strcmp(line, filename) == 0) {
      fprintf(stderr, "ERROR:  File %s has already been added.\n", filename);
      fclose(findex);
      fclose(fnewindex);
      fs_rm(".beargit/.newindex");
      return 3;
    }

    fprintf(fnewindex, "%s\n", line);
  }

  fprintf(fnewindex, "%s\n", filename);
  fclose(findex);
  fclose(fnewindex);

  fs_mv(".beargit/.newindex", ".beargit/.index");

  return 0;
}

/* beargit status
 *
 * See "Step 1" in the project spec.
 *
 */

int beargit_status() {
  /* COMPLETE THE REST */
  FILE* findex = fopen(".beargit/.index", "r");
  
  int count = 0;
  char line[FILENAME_SIZE];
  
  fprintf(stdout, "Tracked files:\n\n");
  while(fgets(line, sizeof(line), findex)) {
    strtok(line, "\n");
    fprintf(stdout, "%s\n", line);
    count++;
  }
  fprintf(stdout, "\nThere are %d files total.\n", count);
  fclose(findex);
  
  return 0;
}

/* beargit rm <filename>
 *
 * See "Step 2" in the project spec.
 *
 */

int beargit_rm(const char* filename) {
  /* COMPLETE THE REST */
  FILE *findex = fopen(".beargit/.index", "r");
  FILE *fnewindex = fopen(".beargit/.newindex", "w");

  char line[FILENAME_SIZE];
  int flag_track = 0;

  while(fgets(line, sizeof(line), findex)) {
    strtok(line, "\n");
    if (strcmp(line, filename) == 0) {
      flag_track = 1;
      continue;
    }
    fprintf(fnewindex, "%s\n", line);
  }
  if (!flag_track) {
      fprintf(stderr, "ERROR:  File %s not tracked.\n", filename);
      fclose(findex);
      fclose(fnewindex);
      fs_rm(".beargit/.newindex");
      return 1;
  }
  
  fclose(findex);
  fclose(fnewindex);

  fs_mv(".beargit/.newindex", ".beargit/.index");
  return 0;
}


int len(const char *str){
  int len=0;
  while((*str++)!='\0')
    len++;
  return len;
}

char *join(const char *a, const char *b) {  
    char *c = (char*)malloc(len(a) + len(b) + 1);
    char *ret = c;  
    while (*a != '\0')
        *c++ = *a++;
    while ((*c++ = *b++) != '\0'){;}
    return ret;  
} 

char *cpy(const char *source, char *des){
  char *r = des;
  while((*des++ = *source++)!='\0');
  return r;
}

/* beargit commit -m <msg>
 *
 * See "Step 3" in the project spec.
 *
 */

const char* go_bears = "THIS IS BEAR TERRITORY!";

int is_commit_msg_ok(const char* msg) {
  /* COMPLETE THE REST */
  int n = 0, m = 0;
  while(msg[m] != '\0'){
    if (go_bears[n] == msg[m]) n++;
    else n=0;
    m++;
    if (n == 23) return 1;
  }
  return 0;
}

/* Use next_commit_id to fill in the rest of the commit ID.
 *
 * Hints:
 * You will need a destination string buffer to hold your next_commit_id, before you copy it back to commit_id
 * You will need to use a function we have provided for you.
 */

void next_commit_id(char* commit_id) {
  /* COMPLETE THE REST */
  char buffer[COMMIT_ID_SIZE];
  char branch_n[BRANCHNAME_SIZE];
  read_string_from_file(".beargit/.current_branch", branch_n, BRANCHNAME_SIZE);
  cryptohash(join(branch_n,commit_id), buffer);
  commit_id = cpy(buffer, commit_id);
}

int beargit_commit(const char* msg) {
  if (!is_commit_msg_ok(msg)) {
    fprintf(stderr, "ERROR:  Message must contain \"%s\"\n", go_bears);
    return 1;
  }
  char current_branch[BRANCHNAME_SIZE];
  read_string_from_file(".beargit/.current_branch", current_branch, BRANCHNAME_SIZE);
  if (strlen(current_branch) == 0){
    fprintf(stderr, "ERROR:  Need to be on HEAD of a branch to commit.\n");
    return 1;
  }

  char commit_id[COMMIT_ID_SIZE];
  read_string_from_file(".beargit/.prev", commit_id, COMMIT_ID_SIZE);
  next_commit_id(commit_id);

  /* COMPLETE THE REST */
  char *temp = join(".beargit/", commit_id);
  fs_mkdir(temp);
  char *newid_dir = join(temp, "/");
  
  fs_cp(".beargit/.index", join(newid_dir, ".index"));
  fs_cp(".beargit/.prev", join(newid_dir, ".prev"));
  
  FILE* findex = fopen(".beargit/.index", "r");
  char line[FILENAME_SIZE];
  while(fgets(line, sizeof(line), findex)) {
    strtok(line, "\n");
    fs_cp(line, join(newid_dir, line));
  }
  fclose(findex);

  write_string_to_file(join(newid_dir,".msg"), msg);
  
  write_string_to_file(".beargit/.prev", commit_id);
  return 0;
}

/* beargit log
 *
 * See "Step 4" in the project spec.
 *
 */

int beargit_log(int limit) {
  /* COMPLETE THE REST */
  int count = 0;
  char commit_id[COMMIT_ID_SIZE];
  read_string_from_file(".beargit/.prev", commit_id, COMMIT_ID_SIZE);
  char *dir = join(".beargit/", commit_id);

  if(!fs_check_dir_exists(dir)){
    fprintf(stderr, "ERROR:  There are no commits.\n");
    return 1;
  }

  while (count < limit){
    dir = join(".beargit/", commit_id);
    if (!fs_check_dir_exists(dir)) break;
    fprintf(stdout, "commit %s\n", commit_id);
    char commit_msg[MSG_SIZE];
    read_string_from_file(join(dir, "/.msg"), commit_msg, MSG_SIZE);
    fprintf(stdout, "   %s\n\n", commit_msg);
    read_string_from_file(join(dir, "/.prev"), commit_id, COMMIT_ID_SIZE);
    count++;
  }
  return 0;
}


// This helper function returns the branch number for a specific branch, or
// returns -1 if the branch does not exist.
int get_branch_number(const char* branch_name) {
  FILE* fbranches = fopen(".beargit/.branches", "r");

  int branch_index = -1;
  int counter = 0;
  char line[BRANCHNAME_SIZE];
  while(fgets(line, sizeof(line), fbranches)) {
    strtok(line, "\n");
    if (strcmp(line, branch_name) == 0) {
      branch_index = counter;
    }
    counter++;
  }

  fclose(fbranches);

  return branch_index;
}

/* beargit branch
 *
 * See "Step 5" in the project spec.
 *
 */

int beargit_branch() {
  /* COMPLETE THE REST */
  FILE* fbranches = fopen(".beargit/.branches", "r");

  char line[BRANCHNAME_SIZE];
  char current_branch[BRANCHNAME_SIZE];
  read_string_from_file(".beargit/.current_branch", current_branch, BRANCHNAME_SIZE);
  int current_branch_index = get_branch_number(current_branch);
  int count = 0;

  while(fgets(line, sizeof(line), fbranches)) {
    strtok(line, "\n");
    if (count != current_branch_index)
      fprintf(stdout, "   %s\n", line);
    else
      fprintf(stdout, "*  %s\n", line);
    count++;
  }

  fclose(fbranches);

  return 0;
}

/* beargit checkout
 *
 * See "Step 6" in the project spec.
 *
 */

int checkout_commit(const char* commit_id) {
  /* COMPLETE THE REST */
  if (strcmp(commit_id, "0000000000000000000000000000000000000000") == 0) {
    write_string_to_file(".beargit/.index", "");
  } 
  else {
    FILE* findex = fopen(".beargit/.index", "r");
    char line[BRANCHNAME_SIZE];
    
    while(fgets(line, sizeof(line), findex)) {
      strtok(line, "\n");
      fs_rm(line);
    }
    fclose(findex);
    
    char commit_index_dir[150];
    sprintf(commit_index_dir, "%s%s%s", ".beargit/", commit_id, "/.index");
    
    FILE* last = fopen(commit_index_dir, "r");
    char new_line[BRANCHNAME_SIZE];
    while(fgets(new_line, sizeof(new_line), last)) {
      strtok(new_line, "\n");
      char final_place[200];
      sprintf(final_place, ".beargit/%s/%s", commit_id, new_line);
      fs_cp(final_place, new_line);
    }
    fs_cp(commit_index_dir, ".beargit/.index");
    fclose(last);
  }

  write_string_to_file(".beargit/.prev", commit_id);
  return 0;
}

int is_it_a_commit_id(const char* commit_id) {
  /* COMPLETE THE REST */
  if (fs_check_dir_exists(join(".beargit/", commit_id)))
    return 1;
  else 
    return 0;
}

int beargit_checkout(const char* arg, int new_branch) {
  // Get the current branch
  char current_branch[BRANCHNAME_SIZE];
  read_string_from_file(".beargit/.current_branch", current_branch, BRANCHNAME_SIZE);

  // If not detached, leave the current branch by storing the current HEAD into that branch's file...
  if (strlen(current_branch) && !new_branch) {
    char current_branch_file[BRANCHNAME_SIZE+50];
    sprintf(current_branch_file, ".beargit/.branch_%s", current_branch);
    fs_cp(".beargit/.prev", current_branch_file);
  }

   // Check whether the argument is a commit ID. If yes, we just change to detached mode
  // without actually having to change into any other branch.
  if (is_it_a_commit_id(arg)) {
    //char commit_dir[FILENAME_SIZE] = ".beargit/";
    //strcat(commit_dir, arg);
    
    // ...and setting the current branch to none (i.e., detached).
    write_string_to_file(".beargit/.current_branch", "");

    return checkout_commit(arg);
  }

  // Read branches file (giving us the HEAD commit id for that branch).
  int branch_exists = (get_branch_number(arg) >= 0);

  // Check for errors.
  if (branch_exists && new_branch) {
    fprintf(stderr, "ERROR:  A branch named %s already exists.\n", arg);
    return 1;
  } else if (!branch_exists && !new_branch) {
    fprintf(stderr, "ERROR:  No branch or commit %s exists.\n", arg);
    return 1;
  }

  // Just a better name, since we now know the argument is a branch name.
  const char* branch_name = arg;

  // File for the branch we are changing into.
  char* branch_file = join(".beargit/.branch_", branch_name);

  // Update the branch file if new branch is created (now it can't go wrong anymore)
  if (new_branch) {
    FILE* fbranches = fopen(".beargit/.branches", "a");
    fprintf(fbranches, "%s\n", branch_name);
    fclose(fbranches);
    fs_cp(".beargit/.prev", branch_file);
  }

  write_string_to_file(".beargit/.current_branch", branch_name);

  // Read the head commit ID of this branch.
  char branch_head_commit_id[COMMIT_ID_SIZE];
  read_string_from_file(branch_file, branch_head_commit_id, COMMIT_ID_SIZE);

  // Check out the actual commit.
  return checkout_commit(branch_head_commit_id);
}

/* beargit reset
 *
 * See "Step 7" in the project spec.
 *
 */

int addFileToIndex(const char *filename, char *dir) {
  char *index_dir = join(dir,"/.index");
  char *newindex_dir = join(dir,"/.newindex");
  
  FILE *findex = fopen(index_dir, "r");
  FILE *fnewindex = fopen(newindex_dir, "w");

  char line[FILENAME_SIZE];
  int flag_exist = 0;

  while(fgets(line, sizeof(line), findex)) {
    strtok(line, "\n");
    if (strcmp(line, filename) == 0) {
      fclose(findex);
      fclose(fnewindex);
      fs_rm(newindex_dir);
      return 1;
    }
    fprintf(fnewindex, "%s\n", line);
  }

  fprintf(fnewindex, "%s\n", filename);
  fclose(findex);
  fclose(fnewindex);

  fs_mv(newindex_dir, index_dir);
  return 0;
}


int beargit_reset(const char* commit_id, const char* filename) {
  if (!is_it_a_commit_id(commit_id)) {
      fprintf(stderr, "ERROR:  Commit %s does not exist.\n", commit_id);
      return 1;
  }
  char *commit_dir = join(".beargit/", commit_id);
  char *commit_dir_slash = join(commit_dir, "/");
  char *commit_index = join(commit_dir, "/.index"); 
  
  FILE* findex = fopen(commit_index, "r");

  char line[FILENAME_SIZE];
  while(fgets(line, sizeof(line), findex)) {
    strtok(line, "\n");
    if (strcmp(line, filename) == 0) {
      fs_cp(join(commit_dir_slash, line), line); 
      fclose(findex);
      
      addFileToIndex(filename, ".beargit");
      return 0;
    }
  }
  fclose(findex);
  fprintf(stderr, "ERROR:  %s is not in the index of commit %s.\n", filename, commit_id);
  return 1;
  

  // Check if the file is in the commit directory
  /* COMPLETE THIS PART */

  // Copy the file to the current working directory
  /* COMPLETE THIS PART */

  // Add the file if it wasn't already there
  /* COMPLETE THIS PART */
}

/* beargit merge
 *
 * See "Step 8" in the project spec.
 *
 */

int beargit_merge(const char* arg) {
  int flag_commit = 0;
  char commit_id[COMMIT_ID_SIZE];
  if (!is_it_a_commit_id(arg)) {
      if (get_branch_number(arg) == -1) {
            fprintf(stderr, "ERROR:  No branch or commit %s exists.\n", arg);
            return 1;
      }
      char branch_file[FILENAME_SIZE];
      snprintf(branch_file, FILENAME_SIZE, ".beargit/.branch_%s", arg);
      read_string_from_file(branch_file, commit_id, COMMIT_ID_SIZE);
  } else {
      flag_commit = 1;
      snprintf(commit_id, COMMIT_ID_SIZE, "%s", arg);
  }

  // Iterate through each line of the commit_id index and determine how you
  // should copy the index file over
   /* COMPLETE THE REST */
  char *dir = join(".beargit/", commit_id);
  char *dir_slash = join(dir, "/");
  char *index_dir = join(dir_slash, ".index");
  FILE *findex = fopen(index_dir, "r");
  
  char current_branch[BRANCHNAME_SIZE];
  read_string_from_file(".beargit/.current_branch", current_branch, BRANCHNAME_SIZE);
  if (strcmp(current_branch, "master") != 0 && strlen(current_branch) != 0){
    char current_branch_commit[COMMIT_ID_SIZE];
    read_string_from_file(join(".beargit/.branch_",current_branch), current_branch_commit, COMMIT_ID_SIZE);
    char *current_dir = join(".beargit/", current_branch_commit);
    char *current_dir_slash = join(current_dir, "/");

    char line[FILENAME_SIZE];
    while(fgets(line, sizeof(line), findex)) {
      strtok(line, "\n");
      int flag_exist = addFileToIndex(line, current_dir);
      if (flag_exist == 0){
        fprintf(stdout, "%s added\n", line);
        fs_cp(join(dir_slash, line), join(current_dir_slash, line));
      }
      else{
        fprintf(stdout, "%s conflicted copy created\n", line);
        fs_cp(join(dir_slash, line), join(join(join(current_dir_slash, line), "."), commit_id));
      }
    }
  }
  else {
    char line[FILENAME_SIZE];
    while(fgets(line, sizeof(line), findex)) {
      strtok(line, "\n");
      int flag_exist = addFileToIndex(line, ".beargit");
      if (flag_exist == 0){
        fprintf(stdout, "%s added\n", line);
        fs_cp(join(dir_slash, line), line);
      }
      else{
        fprintf(stdout, "%s conflicted copy created\n", line);
        fs_cp(join(dir_slash, line), join(join(line, "."), commit_id));
      }
    }
    fclose(findex);
  }
  return 0;
}